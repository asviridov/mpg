-- Selects argument values and related data
-- from FADN database

SELECT
  mp.start_date AS interval_start_dt,
  mp.end_date AS interval_end_dt,
  CASE
    WHEN mpt.name = 'месяц'
      THEN 'month'
    WHEN mpt.name = 'квартал'
      THEN 'quarter'
    WHEN mpt.name = 'полугодие'
      THEN 'half-year'
    WHEN mpt.name = 'год'
      THEN 'year'
    END AS interval_type,
  mev.full_oktmo_code AS region_oktmo,
  m.metric_id AS argument,
  mev.value AS "value",
  mut.unit_type_id AS uom
FROM
  fadn_metric_values AS mv
  LEFT JOIN fadn_metric_external_values AS mev
    ON mev.value_id = mv.value_id
  LEFT JOIN fadn_metric_periods AS mp
    ON mp.metric_period_id = mv.metric_period_id
  LEFT JOIN fadn_metric_period_type AS mpt
    ON mpt.period_type_id = mp.period_type_id
  LEFT JOIN fadn_metric AS m
    ON m.metric_id = mv.metric_id    
  LEFT JOIN fadn_metric_unit_type AS mut
    ON mut.unit_type_id = m.unit_type_id
WHERE
--  mv.creation_date::DATE BETWEEN '2015-11-01'::DATE AND '2015-12-01'::DATE
  mv.creation_date::DATE BETWEEN '{interval_date_start}' AND '{interval_date_end}'

