#! coding: utf-8
import logging
from converter_settings import ZABBIX_LOGGER_FILE_PATH


def setup_logger(logger_name, log_file, level=logging.INFO, log_to_screen=True):
    formatter = logging.Formatter('[%(asctime)-10s]: %(message)s')
    fileHandler = logging.FileHandler(log_file, mode='w')
    fileHandler.setFormatter(formatter)

    l = logging.getLogger(logger_name)
    l.setLevel(level)
    l.addHandler(fileHandler)

    if log_to_screen:
        streamHandler = logging.StreamHandler()
        streamHandler.setFormatter(formatter)
        l.addHandler(streamHandler)


setup_logger('zbx', ZABBIX_LOGGER_FILE_PATH)
zabbix_logger = logging.getLogger('zbx')


def make_log_message_format(to_verify, foo):
    code = foo.__name__
    message = u"[{}]".format(code) + u":[{}]"
    return message


def formatted_log(to_verify=False):
    def wrap(f):
        def wrapper(*args, **kwargs):
            res = None
            message = make_log_message_format(to_verify, f)
            try:
                res = f(*args, **kwargs)
                if to_verify:
                    left, right = res
                    message += ":[{}]:[{}]".format(left, right)
                zabbix_logger.info(message.format("1"))
            except Exception, e:
                zabbix_logger.error(message.format("0"))
                message = "[{}]".format(e)
                zabbix_logger.error(message)
            return res
        return wrapper
    return wrap
