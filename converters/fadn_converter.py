#! coding: utf-8
from tornado import httpclient

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base

import re
import json
import sys
from os.path import abspath, pardir, join, dirname
sys.path.append(abspath(join(dirname(__file__), pardir)))

import settings


"""
HTTP API

 Intervals
-----------

REQ: POST /api/intervals
body: json
[
    {
        "begin": begin_date,
        "end": end_date,
        "type": "year" | "month" | "" | ""
    },
    ...
]

For each interval in body checks its existence. If interval is not existent -
creates it.
Returns all intervals, specified in request (both already existed and new).

RESP: json
[
    {
        "id": id,
        "start_dt": start_dt,
        "end_dt": end_dt,
        "type": "year" | "month" | ...
    },
    ...
]


 UOMs
---------
REQ: GET /api/uoms/check?foreign_id=123,456,789

RESP: json
{
    "existent": [123, 456],
    "non-existent": [789]
}


REQ: POST /api/uoms
body: json
[
    {
        "name": "name",
        "foreign_id": foreign_id,
    }
]
RESP:
    HTTP code:
        200 - ok
        500 - import HTTP error
    body: info about error.


 Regions
---------
REQ: GET /api/regions/check?oktmo=123,456,789

RESP: json
{
    "existent": [123, 456],
    "non-existent": [789]
}


REQ: POST /api/regions
body: json
[
    {
        "name": "name",
        "code": oktmo,
        "region_type": "type"
    }
]
RESP:
    HTTP code:
        200 - ok
        500 - import HTTP error
    body: info about error.


 Arguments
-----------

REQ: GET /api/arguments/check?foreign_id=1,4,7
RESP: json
{
    "existent": [1, 4],
    "non-existent": [7]
}


REQ: POST /api/arguments
body: json
[
    {
        "name": "name",
        "foreign_id": foreign_id,
        "uom": "uom"
        // ??? Which additional arguments should be included?
    },
    ...
]
RESP: json
[
    {
        "id": id,
        "name": "name",
        "foreign_id": foreign_id,
    },
    ...
]


 Argument values
-----------------

REQ: POST /api/argument_values
body: json
[
    {
        "interval_id": interval_id,
        "oktmo": oktmo,
        "argument_foreign_id": argument_foreign_id,
        "value": "value",
    },
    ...
]
RESP:
    HTTP code:
        200 - ok
        500 - import HTTP error
    body: info about error.

"""


# Connection to the database
Base = declarative_base()
engine = create_engine(settings.CONNECTIONS['fadn'])
Base.metadata.bind = engine
metadata = Base.metadata
Session = scoped_session(sessionmaker(bind=engine, autocommit=True))
session = Session()


class DBHelper:
    def execute_from_file(self, filename="", params={}):
        with open(filename, 'r') as query_file:
            try:
                query = query_file.read()
                query = re.sub(r"--.*", "", query)
                query = query.replace('\n', ' ')
                query = query.format(**params)
                obj = session.execute(query)
                return obj.fetchall()

            except Exception as e:
                """
                file_logger.error(e)
                file_logger.error('Cannot read file: {}'.format(query_file.name))
                """
                return e


class Hashable():
    def AttrsToCortage(self):
        attrs = [a for a in dir(self) if not a.startswith('__') and not callable(getattr(self, a))]
        res = ()
        for key in attrs:
            res = res + (getattr(self, key), )
        return res

    def __hash__(self):
        return hash(self.AttrsToCortage())

    def __eq__(self, other):
        return other and self.AttrsToCortage() == other.AttrsToCortage()

    def __ne__(self, other):
        return not self.__eq__(other)


class Interval(Hashable):
    start_dt = ""
    end_dt = ""
    interval_type = ""

    def __init__(self, start_dt, end_dt, interval_type):
        self.start_dt = start_dt
        self.end_dt = end_dt
        self.interval_type = interval_type
        return

    def __repr__(self):
        return "({}, {}, {})".format(self.start_dt, self.end_dt, self.interval_type)


class Importer:
    """
    Base importer class. Holds main import logic.
    """
    filename = ""
    http_client = None
    request_base = "http://localhost:8888/api/{resource}"
    resource = ""

    def __init__(self, http_client):
        self.http_client = http_client

    def read(self, request_params={}):
        """
        Reads specified data from database.
        By default - loads data by runnung SQL query from file.
        """
        db = DBHelper()
        return db.execute_from_file(self.filename, request_params)

    def write(self, data):
        """
        Writes data. By default - HTTP REST api
        """
        import tornado.web
        write_url = self.request_base.format(resource=self.resource)
        res = self.http_client.fetch(write_url,
                                     method="POST",
                                     body=json.dumps(data))
        return tornado.escape.json_decode(res.body)

    def preprocess(self, request_filter):
        """
        Filters out unnecessary items in request_filter
        """
        return request_filter

    def run_import(self, request_filter):
        """
        Main import logic:
        - preprocess (if needed)
        - read data by calling 'read'
        - check consistency by calling 'process'
        - write data by calling 'write'
        """
        request_filter = self.preprocess(request_filter)
        data = self.read(request_filter)
        data = self.process(data)
        return self.write(data)

    def process(self, data=[]):
        """
        Runs necessary checks on data to ensure data consistency.
        E.g. creates related items in other tables, etc.
        """
        return data


class IntervalImporter(Importer):
    filename = ''
    resource = 'intervals'


class UOMImporter(Importer):
    filename = ''
    resource = 'uoms'

    def preprocess(self, request_filter):
        return request_filter


class RegionImporter(Importer):
    filename = ''
    resource = 'regions'

    def preprocess(self, request_filter):
        return request_filter


class IntervalList():
    interval_set = set()
    interval_ids = []

    def save(self, http_client):
        interval_importer = IntervalImporter(http_client)
        intervals_dict = [{
            "end_dt": interval.start_dt,
            "start_dt": interval.end_dt,
            "type": interval.interval_type} for interval in self.interval_set]
        interval_list = interval_importer.write(intervals_dict)
        self.interval_ids = {
            "{}{}{}".format(item["start_dt"], item["end_dt"], item["type"]): item["id"] for item in interval_list}

        return self.interval_ids

    def add(self, **kwargs):
        self.interval_set.add(Interval(**kwargs))
        return

    def get_interval_id(self, start_dt, end_dt, interval_type):
        key = "{}{}{}".format(start_dt, end_dt, interval_type)
        if key in self.interval_ids:
            return self.interval_ids[key]
        return None


class ArgValuesImporter(Importer):
    """
    Importer implementation for ArgumentValues
    """
    filename = 'converters/fadn_sql/arg_values.sql'
    resource = 'argument_values'

    def prepare_dict(self, data, intervals):
        """
        Prepares data to write to the server:
            serializes data to dict, expected by server
            patches IntervalID's
        """
        res = []
        for item in data:
            res.append()
        return data

    def process(self, data=[]):
        intervals = IntervalList()
        regions = set()
        arguments = set()
        uoms = set()

        # for interval_start_dt, interval_end_dt, interval_type, region_oktmo, argument, value, uom in data:
        for item in data:
            intervals.add(**{
                "start_dt": str(item.interval_start_dt),
                "end_dt": str(item.interval_end_dt),
                "interval_type": item.interval_type})
            regions.add(item.region_oktmo)
            arguments.add(item.argument)
            uoms.add(item.uom)

        intervals.save(self.http_client)
        # create intervals on server
        # create regions on server
        # create arguments on server
        # create OUM's on server

        # now all related entities created on server - we can safely post values
        # to the server
        # update interval id's by values, returned by server
        return [{"interval_id": intervals.get_interval_id(
                    item.interval_start_dt,
                    item.interval_end_dt,
                    item.interval_type),
                 "oktmo": item.region_oktmo,
                 "argument_foreign_id": item.argument,
                 "value": item.value} for item in data]


if __name__ == "__main__":
    http_client = httpclient.HTTPClient()

    arg_importer = ArgValuesImporter(http_client)
    res = arg_importer.run_import({'interval_date_start': '2015-11-01',
                                   'interval_date_end': '2015-12-01'})
    http_client.close()
    # print(res)
