CREATE TABLE public.arguments (
      id SERIAL PRIMARY KEY NOT NULL,
      name CHARACTER VARYING(1024) NOT NULL,
      code CHARACTER VARYING(128),
      type CHARACTER VARYING(128),
      uom CHARACTER VARYING(64),
      decimals SMALL INTEGER default 0,
      default_message CHARACTER VARYING(1024),
      function CHARACTER VARYING(64),
      created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
      updated_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
      is_deleted BOOLEAN NOT NULL DEFAULT false
    );

CREATE TABLE public.regions (
      id SERIAL PRIMARY KEY NOT NULL,
      name CHARACTER VARYING(1024) NOT NULL,
      code CHARACTER VARYING(128),
      region_type CHARACTER VARYING(128),
      created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
      updated_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
      is_deleted BOOLEAN NOT NULL DEFAULT false
    );

CREATE TABLE public.intervals (
      id SERIAL PRIMARY KEY NOT NULL,
      start_dt CHARACTER VARYING(1024) NOT NULL,
      end_dt CHARACTER VARYING(128),
      interval_type CHARACTER VARYING(128),
      created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
      updated_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
      is_deleted BOOLEAN NOT NULL DEFAULT false
    );

