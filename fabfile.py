#! coding: utf-8

from fabric.decorators import hosts
from fabric.context_managers import cd, lcd
from fabric.api import run, env, sudo, task, local



@task
@hosts('root@172.17.10.23')
def create_db():
    env.use_ssh_config = True
    run('docker build .')
