#! coding: utf-8
DB_CONN_TMPL = 'postgresql+psycopg2://%(username)s:%(password)s@%(host)s:%(port)s/%(db)s'

DB_FADN_PARAMS = {'username': 'viewer', 'password': 'Hyge783ige',
                  'db': 'fadn', 'host': '185.98.83.33', 'port': 5432}

DB_APP_PARAMS = {'username': 'pgm_user', 'password': 'qwerty123',
                 'db': 'pgm', 'host': '172.17.10.23', 'port': 5432}

CONNECTIONS = {
    'fadn': DB_CONN_TMPL % DB_FADN_PARAMS,
    'app': DB_CONN_TMPL % DB_APP_PARAMS,
}

CONNECTION_POOL_FOR_SESSION = 20
CONNECTION_MAX_OVERFLOW = 0
