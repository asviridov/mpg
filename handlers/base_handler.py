#! coding: utf-8

from tornado.web import RequestHandler


class BaseHandler(RequestHandler):

    def set_default_headers(self):
        self.add_headerset_header("Access-Control-Allow-Origin", "*")
        self.add_headerset_header("Access-Control-Allow-Credentials", "true")
        self.add_headerset_header("Access-Control-Allow-Methods", "GET,POST,PUT")
        self.add_headerset_header("Access-Control-Allow-Headers", "Content-Type, Authorization, Accept")


