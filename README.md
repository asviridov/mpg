Модуль построения графиков

# virtualenv
mkvirtualenv --python=`which python3` mpg 

# current sandbox


# docker 
docker build -t pgm_image .
docker run --name pgm_postgres -e POSTGRES_PASSWORD=mysecretpassword -p 5432:5432 -d pgm_image

workon mpg


