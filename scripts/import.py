#! coding: utf-8

import logging
#import ipdb; ipdb.set_trace()
from models.db import SessionHelper
from models import Arguments, Uoms, ArgumentRelations, Regions


def run():
    sh = SessionHelper()
    fadn_session = sh.get_session_by_term('fadn')
    app_session = sh.get_prime_session()
    uoms = {}
    arguments = {}

    def query():
        q = """
        SELECT
          m.metric_id AS id,
          m.full_name AS full_name,
          m.short_name AS short_name,
          m.unit_type_id AS uom_id
        FROM
          fadn_metric AS m
        ORDER BY
          m.metric_id
        """

        res = fadn_session.execute(q.replace("\n", ""))
        return res.fetchall()

    def query_argument_relations():
        q = """
        SELECT
          m.parent_id AS parent_argument_id,
          m.metric_id AS child_argument_id
        FROM
          fadn_metric AS m
        WHERE
          m.parent_id IS NOT NULL
        ORDER BY
          m.parent_id
        """
        res = fadn_session.execute(q.replace("\n", ""))
        return res.fetchall()

    def query_regions():
        q = """
        SELECT
          uo.full_oktmo_code AS oktmo,
          uo.oktmo_name AS full_name,
          uo.oktmo_note AS short_name
        FROM
          fadn_unidic_oktmo as uo
        WHERE
          uo.is_deleted IS NOT TRUE
        ORDER BY
          uo.full_oktmo_code

        """
        res = fadn_session.execute(q.replace("\n", ""))
        return res.fetchall()

    def get_uoms():
        if not uoms:
            objects = app_session.query(Uoms).all()
            for o in objects:
                uoms[o.foreign_id] = o.id
        return uoms

    def create_argument(row):
        foreign_id, full_name, short_name, ext_uom_id = row

        uom_id = get_uoms()[str(ext_uom_id)]

        return Arguments(foreign_id=foreign_id,
                         full_name=full_name,
                         short_name=short_name,
                         uom_id=uom_id)

    def create_region(row):
        oktmo, full_name, short_name = row
        return Regions(full_name, short_name, oktmo)

    def get_arguments():
        if not arguments:
            args = app_session.query(Arguments).all()
            for arg in args:
                arguments[arg.foreign_id] = arg.id
        return arguments

    def create_argument_relation(row):
        parent_argument_id, child_argument_id = row
        parent_arg = get_arguments()[str(parent_argument_id)]
        child_arg = get_arguments()[str(child_argument_id)]
        return ArgumentRelations(parent_arg, child_arg)

    res = []
    for line in query_regions():
        l = [el.strip() if isinstance(el, str) else el for el in line]
        res.append(l)
    import ipdb; ipdb.set_trace()
    logging.info("Got {} results from FADN database".format(len(res)))

    objs = []
    for rel in res:
        objs.append(create_region(rel))

    import ipdb; ipdb.set_trace()
    app_session.add_all(objs)
    app_session.commit()




if __name__ == "__main__":
    run()
