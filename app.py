#! coding: utf-8

import logging
import json
import tornado.ioloop
import tornado.web
from tornado.log import app_log as log
from tornado.options import define, options, parse_command_line
from tornado.httpserver import HTTPServer


define("listen", default='localhost', help="run on the given host")
define("port", default=8888, help="run on the given port", type=int)
define("debug", default=True, help="Debug", type=bool)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Nothing added yet")


class EchoHandler(tornado.web.RequestHandler):
    def get(self):
        log.info("Query: {}".format(self.request.query))
        self.write("Get request processed")

    def post(self):
        def add_id(item):
            import random
            item["id"] = random.randint(1, 100)
            return item

        log.info("Body: {}".format(self.request.body))
        data = tornado.escape.json_decode(self.request.body)
        res = [add_id(item) for item in data]
        self.write(json.dumps(res))


def make_application():
    return tornado.web.Application([
        (r'/', MainHandler),
        (r'/api/.*', EchoHandler)
        ],
        debug=options.debug,
        reload=options.debug
    )


def runserver():
    parse_command_line()
    app = make_application()
    HTTPServer(app).listen(options.port)
    log.info("MPG has been started on {}:{}".format(options.listen, options.port))
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    runserver()
