from sqlalchemy import (BigInteger, Boolean, DateTime, ForeignKey, Integer, Numeric,
                        SmallInteger, String, Text, Index, Interval)
from sqlalchemy import Column
from datetime import datetime
from .db import Base


class Versioning(object):
    id = Column(BigInteger, nullable=False, primary_key=True, autoincrement=True, unique=True, index=True)
    created_at = Column(DateTime, default=datetime.utcnow())
    updated_at = Column(DateTime, default=datetime.utcnow())
    is_deleted = Column(Boolean, default=False)


class Sources(Base, Versioning):
    __tablename__ = 'sources'
    full_name = Column(String(1024), nullable=False, unique=True, index=True)
    short_name = Column(String(128), nullable=True)


class Arguments(Base, Versioning):
    __tablename__ = "arguments"
    full_name = Column(String(1024), nullable=True, index=True)
    short_name = Column(String(512), nullable=True)
    code = Column(String(128))
    type = Column(String(128))
    uom = Column(String(64))
    decimals = Column(SmallInteger)
    default_message = Column(String(1024))
    description = Column(String(1024))
    function = Column(String(128))
    foreign_id = Column(String(128))
    uom_id = Column(ForeignKey('uoms.id'))
    source = Column(ForeignKey('sources.id'))

    def __init__(self, full_name, short_name, uom_id, foreign_id):
        self.full_name = full_name
        self.short_name = short_name
        self.uom_id = uom_id
        self.foreign_id = foreign_id
        self.type = 'table'


class Regions(Base, Versioning):
    __tablename__ = "regions"
    full_name = Column(String(1024), nullable=True)
    short_name = Column(String(512))
    code = Column(String(128))
    region_type = Column(String(128))
    oktmo = Column(String(64), nullable=False, index=True, unique=True)

    def __init__(self, full_name, short_name, oktmo):
        self.full_name = full_name
        self.short_name = short_name
        self.oktmo = oktmo


class Uoms(Base, Versioning):
    __tablename__ = "uoms"
    foreign_id = Column(String(128))
    full_name = Column(String(256))
    short_name = Column(String(128))
    code = Column(String(128))


class Intervals(Base, Versioning):
    __tablename__ = "intervals"
    start_dt = Column(DateTime, nullable=False)
    end_dt = Column(DateTime, nullable=False)
    interval_type = Column(String(128))


class ArgumentValues(Base, Versioning):
    __tablename__ = "argument_values"
    interval_id = Column(ForeignKey('intervals.id', ondelete='RESTRICT', onupdate='RESTRICT'), index=True)
    region_id = Column(ForeignKey('regions.id', ondelete='RESTRICT', onupdate='RESTRICT'), index=True)
    argument_id = Column(ForeignKey('arguments.id', ondelete='RESTRICT', onupdate='RESTRICT'), index=True)
    value = Column(String(1024), nullable=False, unique=True, index=True)
    #  argument_range_id = Column(ForeignKey('argument_ranges.id', ondelete='RESTRICT', onupdate='RESTRICT'), index=True)
    is_verified = Column(Boolean, nullable=False, default=True)
    is_considered = Column(Boolean, nullable=False, default=True)


class Categories(Base, Versioning):
    __tablename__ = "categories"
    name = Column(String(1024), nullable=False, unique=True, index=True)
    code = Column(String(128))
    category_type = Column(String(128))
    pos = Column(SmallInteger)
    is_visible = Column(Boolean, nullable=False, default=True)
    is_active = Column(Boolean, nullable=False, default=True)


class CategoryArguments(Base, Versioning):
    __tablename__ = "category_arguments"
    category_id = Column(ForeignKey('categories.id', ondelete='RESTRICT', onupdate='RESTRICT'), index=True)
    argument_id = Column(ForeignKey('arguments.id', ondelete='RESTRICT', onupdate='RESTRICT'), index=True)
    position = Column(SmallInteger)
    is_visible = Column(Boolean, nullable=False, default=True)
    is_active = Column(Boolean, nullable=False, default=True)


class ArgumentRanges(Base, Versioning):
    __tablename__ = "argument_ranges"
    argument_id = Column(ForeignKey('arguments.id', ondelete='RESTRICT', onupdate='RESTRICT'), index=True)
    name = Column(String(1024), nullable=False, unique=True, index=True)
    code = Column(String(128))
    range_top = Column(Integer)
    range_bottom = Column(Integer)
    degrees = Column(String(128))
    status = Column(String(128))
    message = Column(String(1024))
    color = Column(String(128))


class ArgumentRelations(Base, Versioning):
    __tablename__ = "argument_relations"
    parent_argument_id = Column(ForeignKey('arguments.id', ondelete='RESTRICT', onupdate='RESTRICT'), index=True)
    child_argument_id = Column(ForeignKey('arguments.id', ondelete='RESTRICT', onupdate='RESTRICT'), index=True)
    relation_type = Column(String(128))
    direction = Column(String(16))
    weight = Column(Integer)
    level = Column(SmallInteger)
    position = Column(SmallInteger)

    def __init__(self, parent_argument_id, child_argument_id):
        self.parent_argument_id = parent_argument_id
        self.child_argument_id = child_argument_id


class CategoryRelations(Base, Versioning):
    __tablename__ = "category_relations"
    parent_category_id = Column(ForeignKey('categories.id', ondelete='RESTRICT', onupdate='RESTRICT'), index=True)
    child_category_id = Column(ForeignKey('categories.id', ondelete='RESTRICT', onupdate='RESTRICT'), index=True)
    relation_type = Column(String(128))
    direction = Column(String(16))
    weight = Column(Integer)
    level = Column(SmallInteger)
    position = Column(SmallInteger)


class ValueRelations(Base, Versioning):
    __tablename__ = "value_relations"
    parent_value_id = Column(ForeignKey('argument_values.id', ondelete='RESTRICT', onupdate='RESTRICT'), index=True)
    child_value_id = Column(ForeignKey('argument_values.id', ondelete='RESTRICT', onupdate='RESTRICT'), index=True)
    relation_type = Column(String(128))
    direction = Column(String(16))
    weight = Column(Integer)
    level = Column(SmallInteger)
    position = Column(SmallInteger)


class Users(Base, Versioning):
    __tablename__ = "users"

    last_name = Column(String(64))
    first_name = Column(String(64))
    middle_name = Column(String(64))
    login = Column(String(256))
    password = Column(String(256))
    email = Column(String(256))
    token = Column(String(128))
    is_active = Column(Boolean)
    is_blocked = Column(Boolean)

    def get_full_name(self):
        return "{} {} {}".format(self.first_name, self.middle_name, self.last_name)

    def change_password(self, old_pass, new_pass):
        if not old_pass:
            return

    def get_user_roles(self):
        pass

# class RoleCategories(Base, Versioning):
#     role_id
#     category_id
#     position
#     is_visible
#     is_active

# class UserRoles(Base, Versioning):
#     user_id
#     role_id
#     position
#     login
#     password
#     is_visible
#     is_active
#     created_at
#     updated_at
#     is_deleted

# class Users(Base, Versioning):
#     name
#     email
#     contact


# class Roles(Base, Versioning):
#     name
#     code
#     type
#     weight
#     is_visible
#     is_active
