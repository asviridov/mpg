#! coding: utf-8

import sys
import settings

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base

from os.path import abspath, pardir, join, dirname
sys.path.append(abspath(join(dirname(__file__), pardir, pardir)))
Base = declarative_base()
metadata = Base.metadata

engine = create_engine(settings.CONNECTIONS['fadn'])
Base.metadata.bind = engine

Session = scoped_session(sessionmaker(bind=engine, autocommit=True))


class SessionHelper(object):

    """
    Grabs all connection strings from settings and make connection pool
    """

    def __init__(self):
        self.available_connections = settings.CONNECTIONS.keys()
        self.prime_conn = None
        if 'app' in self.available_connections:
            self.prime_conn_name = 'app'

        self.sessions = {}
        for conn_name, conn in settings.CONNECTIONS.items():
            self.sessions[conn_name] = self.create_session(conn)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close_all_sessions()

    def get_session_tuple(self, conn_string):
        return self.get_or_create(conn_string), self.get_or_create(settings.DB_CO_CONN)

    def get_prime_session(self):
        return self.get_session_by_term(self.prime_conn_name)

    def get_session_by_term(self, term):
        return self.sessions.get(term)

    def __add_session(self, conn_string):
        session = self.create_session(conn_string)
        self.sessions[conn_string] = session
        return session

    def get_session_or_none(self, conn_string):
        return self.sessions.get(conn_string)

    def create_session(self, conn_string):
        engine = create_engine(conn_string,
                               pool_size=settings.CONNECTION_POOL_FOR_SESSION,
                               max_overflow=settings.CONNECTION_MAX_OVERFLOW)
        Session = sessionmaker(bind=engine)
        return Session()

    def get_or_create(self, conn_string):
        if hasattr(self, 'sessions'):
            session = self.get_session_or_none(conn_string)
            if not session:
                session = self.__add_session(conn_string)
            return session
        else:
            return self.create_session(conn_string)

    def close_session_by_term(self, term):
        session = self.get_session_by_term(term)
        session.commit()
        session.close()

    def close_all_sessions(self):
        for term, session in self.sessions.iteritems():
            session.commit()
            session.close()

    def is_object_pending(self, obj):
        if inspect(obj).pending:
            return True

    def commit(self, term):
        session = self.get_session_by_term(term)
        session.commit()

    def clean_all_sessions(self, rollback=False):
        for term, session in self.sessions.iteritems():
            session.flush()
            if rollback:
                session.rollback()
